<?php
function ubah_huruf($string)
{
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $result = "<br>";
    for ($i = 0; $i < strlen($string); $i++) {
        $position = strrpos($abjad, $string[$i]);
        $result .= substr($abjad, $position + 1, 1);
    }
    return $result;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
