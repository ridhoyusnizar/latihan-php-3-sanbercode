<?php
function tentukan_nilai($number)
{
    $result = "<br>";
    if ($number >= 85 && $number <= 100) {
        $result .= "Sangat Baik";
    } else if ($number >= 70 && $number < 85) {
        $result .= "Baik";
    } else if ($number >= 60 && $number < 70) {
        $result .= "Cukup";
    } else {
        $result .= "Kurang";
    }
    return $result;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
